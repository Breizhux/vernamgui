#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
from datetime import datetime



ORDER_SIZE = {'Octets' : 1, 'O' : 1,
              'K' : 1000,
              'M' : 1000000,
              'G' : 1000000000}



class File :
    """ A class to iterate bytes of file easily,
    and get the size of file in human representation.
    """
    def __init__(self, path, mode='rb'):
        """ Initialize the file with a valid path :
        path : the path for the file.
        mode (rb) : the mode to open the file.
        """
        if not os.path.exists(path) :
            raise("PathError", "Error : path {} does not exists.")
        self.path = path
        self.mode = mode
        self._file = open(self.path, self.mode)

    def __del__(self) :
        self._file.close()

    def __repr__(self) :
        return "<File (path=\"{}\", mode={})>".format(self.path, self.mode)

    def __len__(self) :
        return os.path.getsize(self.path)

    def __iter__(self) :
        byte = True
        while byte :
            byte = self._file.read(1)
            yield byte

    def get_size(self, r=None, join=True) :
        """ Return the human size. If join = False,
        return (size, size_order (Go/Mo/...)).
        """
        size = len(self)
        for i in ORDER_SIZE :
            if 1000 > size / ORDER_SIZE[i] > 0 :
                if r is not None :
                    size = round(size / ORDER_SIZE[i], r)
                else :
                    size = size / ORDER_SIZE[i]
                if not join :
                    return (size, i)
                return "{}{}".format(size, i)
        if not join :
            return (size, "Octets")
        return "{}Octets".format(size)



class Key(File) :
    """ A class manage a vernam key file. Can generate random key,
    read the key, etc...
    """
    def __init__(self, path, length=None) :
        """ Initialize the key file :
        path : the path for the file. If not exists, create it.
        lenght (None) : if defined, add [length] random key to file.
        """
        if not os.path.exists(path) :
            with open(path, 'w') as file : pass
        File.__init__(self, path)
        #The name of the key is the name of the file without extention
        self.name = os.path.splitext(os.path.basename(self.path))[0]
        #if length not None, generate key and append/write it.
        if length is not None : self.generate(length)

    def __repr__(self) :
        return "<Key (path=\"{}\", name=\"{}\", mode={}, size={})>".format(
            self.path, self.name, self.mode, len(self))

    def generate(self, length, mode='a') :
        """ Generate a random key file. You can write on the
        existing file with mode 'w' or append on the existing
        key with the mode 'a'.
        """
        #Try to get the true length to generate
        if isinstance(length, str) :
            try :
                length = int(float(length[:-1]) * ORDER_SIZE[length[-1]])
            except :
                raise("ArgumentError", "Error : length {} not valid. Ex: 10, 4.7M.")
        self._file.close()
        #write key by 10000 bytes so as not to saturate the ram.
        n = length // 10000
        with open(self.path, mode+'b') as file :
            for i in range(n) :
                file.write(os.urandom(10000))
            file.write(os.urandom(length % 10000))
        self._file = open(self.path, self.mode)

    def get_key(self, length) :
        """ Returns a key of the desired length."""
        key = bytes()
        for i, k in enumerate(self) :
            key += k
            if i == length : break
        else :
            raise("SizeError", "Error : the key is less than the required length")
        self.cut(length)
        return key

    def cut(self, index) :
        """ Remove the start of the key if it is used."""
        #prepare a new file to write the existing key.
        tmp_path = "{}/.cuttingkey.tmp".format(os.path.dirname(self.path))
        self._file.close()
        self._file = open(self.path, 'rb')
        #read the key after the index, and write it in tmp file.
        with open(tmp_path, 'wb') as file :
            for i in range(index) :
                byte = self._file.read(1)
                if not byte : break
            while byte :
                byte = self._file.read(1)
                file.write(byte)
        self._file.close()
        #remove last key, and move the new key.
        os.remove(self.path)
        os.rename(tmp_path, self.path)

    def get_date(self) :
        """ Return the latest modification of file."""
        date = os.path.getctime(self.path)
        return datetime.utcfromtimestamp(date).strftime('%Y/%m/%d %H:%M')



def xor(a, b) :
    """ Make the xor computation from two bytes."""
    return bytes(_a ^ _b for _a, _b in zip(a, b))

def xor_file(in_path, key, out_path, **kwargs) :
    """ Encrypt or decrypt file. Require the input
    file, key path, output path.
    """
    close_key = False
    if isinstance(key, str) :
        close_key = True
        key = Key(key_path, **kwargs)
    plain = File(in_path, **kwargs)
    if len(plain) > len(key) :
        raise Exception("The key is less than the file to encrypt.")
    #write the xor operation bytes per bytes
    with open(out_path, 'wb') as crypt :
        for i, k in zip(plain, key) :
            crypt.write(xor(i, k))
    #cut the used key
    key.cut(len(plain))
    #close files
    if close_key : del key
    del plain

def xor_text(in_text, key, **kwargs) :
    """ Encrypt or decrypt a text/binary. Return bytes."""
    #convert input in bytes if necessary
    if not isinstance(in_text, bytes) :
        try :
            bytes_text = str(in_text).encode('utf-8')
        except :
            raise ValueError("Cannot compute this input \"{}\"".format(in_text))
    else :
        bytes_text = in_text
    #Get the key to encrypt text
    close_key = False
    if isinstance(key, str) :
        close_key = True
        key = Key(key_path, **kwargs)
    if len(bytes_text) > len(key) :
        raise Exception("The key is less than the text to encrypt.")
    bytes_key = key.get_key(len(bytes_text))
    if close_key : del key
    #return the xor operation in result string
    return xor(bytes_text, bytes_key)



if __name__ == "__main__" :
    import sys
    if len(sys.argv) > 1 and sys.argv[1] in ["generate", "crypt", "decrypt", "text"] :
        action = sys.argv[1:]
    else :
        action = "generate"

    path_plain = "./img.png"
    path_crypt = "./img.vrnm"
    path_decrypt = "./img.vrnm.png"
    crypt_key = "./crypt.key"
    decrypt_key = "./decrypt.key"

    if "generate" in action :
        print("Generate key...\n\t[key] {}".format(crypt_key))
        key = Key(crypt_key, length="2M")
        del key
        print("\tCopy key...\n\t[key] {}".format(decrypt_key))
        os.system("cp \"{}\" \"{}\"".format(crypt_key, decrypt_key))
        print("Done !")

    elif "crypt" in action :
        print("Crypt a file...\n\t[plain] {}\n\t[key] {}\n\t[crypted] {}".format(
            path_plain, crypt_key, path_crypt))
        xor_file(path_plain, crypt_key, path_crypt)
        print("Done !")

    elif "decrypt" in action :
        print("Decrypt a file...\n\t[crypted] {}\n\t[key] {}\n\t[decrypted] {}".format(
            path_crypt, decrypt_key, path_decrypt))
        xor_file(path_crypt, decrypt_key, path_decrypt)
        print("Done !")

    elif "text" in action :
        plain_text = "Mon texte à chiffrer !"
        print("[plain text] \"{}\"".format(plain_text))
        crypted_text = xor_text(plain_text, crypt_key)
        print("[crypted text] \"{}\"".format(crypted_text))
        decrypt_text = xor_text(crypted_text, decrypt_key).decode('utf-8')
        print("[decrypted text] \"{}\"".format(decrypt_text))
