#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
import pyperclip
from xortool import *
from shutil import move
from time import time, sleep
from threading import Thread
from subprocess import run, Popen, PIPE

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

ORDER = ["Octets", "K", "M", "G"]

class GtkKeySettings :
    """ Gtk Dialog to configure a Vernam Key."""
    LABEL_MSG = {
        "keyalreadyexists" : "Choisissez un autre nom. La clé existe déjà.",
    }
    def __init__(self, gkey, key_folders=None) :
        self.gkey = gkey
        self.key_folders = key_folders
        interface = Gtk.Builder()
        interface.add_from_file("./gi/key_settings.glade")
        self.dialog = interface.get_object("keysettings_dialog")
        self.name = interface.get_object("keyname")
        self.size = interface.get_object("keysize")
        self.order = interface.get_object("keyorder")
        self.type = interface.get_object("keytype")
        self.label = interface.get_object("messagelabel")
        self.savebutton = interface.get_object("savebutton")
        self.name.set_text(self.gkey.name)
        self.initial_size = self.gkey.get_size(join=False)
        if self.key_folders is None :
            self.type.set_visible(0)
        elif self.key_folders[1] in self.gkey.path :
            self.type.set_active(1)
        self.size.set_value(self.initial_size[0])
        self.order.set_active(ORDER.index(self.initial_size[1]))
        interface.connect_signals(self)

    def run(self) :
        self.dialog.run()

    def quit(self, *args) :
        self.dialog.destroy()

    def verify_name(self, *args) :
        path = "{}/{}.key".format(self.key_folders[self.type.get_active()], self.name.get_text())
        if os.path.exists(path) and path != self.gkey.path :
            self.label.set_text(self.LABEL_MSG['keyalreadyexists'])
            self.savebutton.set_sensitive(0)
            return None
        self.label.set_text("")
        self.savebutton.set_sensitive(1)

    def save(self, *args) :
        #Renommage si besoin
        path = "{}/{}.key".format(self.key_folders[self.type.get_active()], self.name.get_text())
        if path != self.gkey.path :
            os.rename(self.gkey.path, path)
            self.gkey.path = path
        #Complétion de la clé aléatoire si besoin
        length = self.size.get_value()
        order = ORDER[self.order.get_active()]
        size = int(length)*ORDER_SIZE[order[0]]
        if size > len(self.gkey) :
            progress = GtkProgressDialog(path, size, text="Génération de la clé en cours...")
            progress.start()
            self.gkey.generate(size - len(self.gkey))
            progress.join()
        self.dialog.destroy()

    def delete_key(self, *args) :
        os.remove(self.gkey.path)
        self.dialog.destroy()



class GtkAddKey :
    """ Gtk Dialog to create or import a Vernam Key.
    Need the defaults path for (entryption key, decryption key)."""
    LABEL_MSG = {
        "keyalreadyexists" : "Choisissez un autre nom. La clé existe déjà.",
    }
    def __init__(self, defaults) :
        self.defaults_paths = defaults
        interface = Gtk.Builder()
        interface.add_from_file("./gi/add_new_key.glade")
        self.dialog = interface.get_object("addnewkey_dialog")
        self.type = interface.get_object("keytype")
        self.name = interface.get_object("keyname")
        self.size = interface.get_object("keysize")
        self.order = interface.get_object("keyorder")
        self.generate_button = interface.get_object("generate")
        self.path = interface.get_object("keypath")
        self.linkkey = interface.get_object("linkkey")
        self.labelsize = interface.get_object("labelsize")
        self.baselabel = self.labelsize.get_text()
        self.label = interface.get_object("messagelabel")
        interface.connect_signals(self)

    def run(self) :
        self.dialog.run()

    def quit(self, *args) :
        self.dialog.destroy()

    def verify_name(self, *args) :
        path = "{}/{}.key".format(
            self.defaults_paths[self.type.get_active()],
            self.name.get_text())
        if os.path.exists(path) :
            self.label.set_text(self.LABEL_MSG['keyalreadyexists'])
            self.generate_button.set_sensitive(0)
            return None
        if self.name.get_text() == "" :
            self.generate_button.set_sensitive(0)
            return None
        self.label.set_text("")
        self.generate_button.set_sensitive(1)

    def generate(self, *args) :
        path = "{}/{}.key".format(
            self.defaults_paths[self.type.get_active()],
            self.name.get_text())
        size = "{}{}".format(self.size.get_value(), ORDER[self.order.get_active()])
        key = Key(path)
        progress = GtkProgressDialog(path, size, text="Génération de la clé en cours...")
        progress.start()
        key.generate(size)
        progress.join()
        del key
        self.dialog.destroy()

    def import_key(self, *args) :
        path = self.path.get_filename()
        output = "{}/{}".format(self.defaults_paths[self.type.get_active()], os.path.basename(path))
        if not self.linkkey.get_active() :
            move(path, output)
        else :
            os.link(path, output)
        self.dialog.destroy()

    def update_infos(self, *args) :
        file = File(self.path.get_filename())
        size = len(file)
        self.labelsize.set_text(file.get_size())
        del file
        #si la taille est > 200Mo, on préconise de créer un lien.
        if size > 200000000 : self.linkkey.set_active(1)



class GtkOutputDialog :
    """ Gtk dialog to ask what to do with the text:
    copy in pyperclip or save in file."""
    def __init__(self, text) :
        self.text = text
        interface = Gtk.Builder()
        interface.add_from_file("./gi/output_dialog.glade")
        self.dialog = interface.get_object("output_dialog")
        self.label = interface.get_object("label")
        self.label.set_text(
            "chaine de caractères" if isinstance(self.text, str) else "Octets")
        self.copy_button = interface.get_object("copy_button")
        if not isinstance(self.text, str) :
            self.copy_button.set_sensitive(0)
        interface.connect_signals(self)

    def quit(self, *args) :
        self.dialog.destroy()

    def run(self) :
        self.dialog.run()

    def save(self, *args) :
        path = GtkFileSaveDialog()
        path.choose()
        path = path.get_filename()
        if isinstance(self.text, str) :
            mode = 'w'
        else :
            mode = 'wb'
            if not path.endswith(".vrnm") :
                path += ".vrnm"
        with open(path, mode) as file :
            file.write(self.text)
        self.dialog.destroy()

    def copy(self, *args) :
        pyperclip.copy(self.text)
        self.dialog.destroy()


class GtkFileSaveDialog :
    """ Gtk3 have no action save for GtkFileChooserButton.
    Use zenity to use this option."""
    OPTIONS = {
        "title" : "Emplacement du fichier chiffré ou déchiffré.",
        "filename" : os.environ['HOME'],
    }
    def __init__(self, label=None) :
        #the parent GtkButton
        self.label = label
        self.command = "zenity --file-selection --confirm-overwrite --save --title \"{}\" --filename \"{}\""
        self._path = None

    def _set_default_options(self, **kwargs) :
        if self._path != "" :
            self.OPTIONS['filename'] = self._path
        for i in self.OPTIONS :
            if i in kwargs :
                self.OPTIONS[i] = kwargs[i]

    def choose(self, **kwargs) :
        self._set_default_options(**kwargs)
        command = self.command.format(self.OPTIONS['title'], self.OPTIONS['filename'])
        process = run(command, stdout=PIPE, shell=True, encoding='utf8')
        if process.stdout == "" :
            self.set_filename(None)
        else :
            self.set_filename(process.stdout[:-1])

    def get_filename(self) :
        return self._path

    def set_filename(self, path) :
        self._path = path
        if self.label is None : return None
        if path is None :
            self.label.set_text("(Aucun)")
        else :
            self.label.set_text(os.path.basename(self._path))



class GtkProgressDialog(Thread) :
    """ Zenity progress dialog for a write file.
    """
    OPTIONS = {
        "title" : "Écriture du fichier en cours...",
    }
    def __init__(self, path, size, **kwargs) :
        Thread.__init__(self)
        self._set_options(**kwargs)
        #destination théorique
        self.path = path
        self.final_size = self._get_size(size)
        #avancement
        self.percent = 0
        self.startdate = time()
        self.eta = 0
        #affichage
        self._process = None
        self.command = "zenity --progress --title=\"{}\" {}".format(
            self.OPTIONS['title'],
            "--width 400 --percentage=0 --auto-close --no-cancel")

    def _set_options(self, **kwargs) :
        for i in self.OPTIONS :
            if i in kwargs :
                self.OPTIONS[i] = kwargs[i]

    def _get_size(self, size) :
        if isinstance(size, str) :
            return int(float(size[:-1]) * ORDER_SIZE[size[-1]])
        return size

    def _set_eta(self) :
        size = os.path.getsize(self.path)
        vitesse = size / (time()-self.startdate)
        if vitesse == 0 :
            self.eta = "calcul du temps restant en cours..."
            return None
        eta = (self.final_size-size) * 1/vitesse
        self.eta = datetime.utcfromtimestamp(eta).strftime('%M:%Ss')

    def run(self) :
        self._process = Popen(self.command, stdin=PIPE, shell=True, encoding='utf-8')
        while not os.path.exists(self.path) and os.path.getsize(self.path) == 0 :
            sleep(0.1)
        while True :
            size = os.path.getsize(self.path)
            self.percent = size*100/self.final_size
            self._set_eta()

            if size == self.final_size :
                self.percent = 100

            try :
                self._process.stdin.write("{}\n".format(round(self.percent)))
                self._process.stdin.write("#Temps restant : {}\n".format(self.eta))
                self._process.stdin.flush()
            except BrokenPipeError:
                break
