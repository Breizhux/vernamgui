# VernamGui

This is a simple Gtk3 interface to encrypt text of files with Vernam protocol.

![Interface VernamGui](./vernam_gui.png "Interface de VernamGui en Gtk3")

## Installation

```
apt install -y python3-gi zenity python3-pyperclip
git clone https://gitlab.com/Breizhux/vernamgui.git
```

## Usage

To start this software, use the following command :
`
python3 "path/to/cloned_repository/vernamgui/vernam.py"
`

## Description

xortool : is the file contain function to read files and key, and compute the xor operation between informations.

vernam : is contain the gui. You can manage keys, encrypt and decrypt file or text. It is writen in Gtk (depend from gi/*.glade).

dialogbox : is contain many dialog box. Depend of zenity linux command.

## ToDo :

☐ Verify if output file exists before crypt operation.

☐ Create the search key functions.

☐ Automatically reposition the GtkPaned when the window is resized.