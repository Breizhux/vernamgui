#!/usr/bin/python3
# -*- coding : utf-8 -*-

import os
from xortool import *
from threading import Thread

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dialogbox import *



class GtkKey(Key) :
    def __init__(self, path) :
        Key.__init__(self, path)
        interface = Gtk.Builder()
        interface.add_from_file("./gi/key.glade")
        self.row = interface.get_object("listrow")
        self.keyname = interface.get_object("name")
        self.keyname.set_text(self.name)
        self.date = interface.get_object("date")
        self.date.set_text(self.date.get_text().replace("$date", self.get_date()))
        self.size = interface.get_object("size")
        self.size.set_text(self.size.get_text().replace("$size", self.get_size(r=2)))
        interface.connect_signals(self)


class VernamGui :
    DEFAULT_ENCRYPT_KEYS = "{}/.config/vernam/encrypt_key".format(os.environ['HOME'])
    DEFAULT_DECRYPT_KEYS = "{}/.config/vernam/decrypt_key".format(os.environ['HOME'])
    LABEL_MESSAGE = {
        "nokeyselected" : "Vous devez sélectionnez une clé pour faire cette action !",
        "nodatainput" : "Vous devez entrez du texte ou choisir un fichier pour commencer à faire des opéations de chiffrements.",
        "computing_is_end" : "Opération terminée !",
        "progress_bar_keyrewriting" : "Réécriture de la clé en cours...",
        "progress_bar_encrypt" : "Opération de chiffrement en cours...",
        "progress_bar_decrypt" : "Opération de déchiffrement en cours...",
        "length_error" : "Erreur, la clé est plus petite que ce qu'il faut chiffrer.",
    }
    LABEL_BASE = {
        "openfilesize" : "Taille du fichier : $size",
        "resultfiledefault" : "Fichier de sortie : $path",
    }

    def __init__(self) :
        self.keys = dict() #{GtkKey.row : GtkKey}
        #Get Gtk Objects
        interface = Gtk.Builder()
        interface.add_from_file("./gi/main.glade")
        self.window = interface.get_object("mainwindow")
        self.window.connect("delete-event", self.main_quit)
        self.encryption_keys = interface.get_object("encryption_keys")
        self.decryption_keys = interface.get_object("decryption_keys")
        self.entry = interface.get_object("textentry")
        self.input = interface.get_object("openfile")
        self.output = GtkFileSaveDialog(interface.get_object("savelabel")) #depend on zenity
        self.outputbutton = interface.get_object("outputbutton") #not available by default
        self.inputlabel = interface.get_object("openfilelabel")
        self.outputlabel = interface.get_object("resultfilelabel")
        self.label = interface.get_object("mainlabel")
        #Set windows components
        self.set_listbox_keys()
        interface.connect_signals(self)
        #Show window
        self.window.show_all()

    def main_quit(self, *args) :
        Gtk.main_quit()

    def filesavedialog(self, *args) :
        """ Open a gtk file dialog in save action."""
        actual = self.output.get_filename()
        if actual is None :
            actual = self.input.get_filename()
        self.output.choose(filename=actual)
        self.update_openfile_infos()

    def set_listbox_keys(self, *args) :
        """ Set the list of keys."""
        for i in self.encryption_keys :
            self.encryption_keys.remove(i)
        for i in self.decryption_keys :
            self.decryption_keys.remove(i)
        self.keys.clear()
        for i in os.listdir(self.DEFAULT_ENCRYPT_KEYS) :
            item = GtkKey("{}/{}".format(self.DEFAULT_ENCRYPT_KEYS, i))
            self.keys[item.row] = item
            self.encryption_keys.add(item.row)
        for i in os.listdir(self.DEFAULT_DECRYPT_KEYS) :
            item = GtkKey("{}/{}".format(self.DEFAULT_DECRYPT_KEYS, i))
            self.keys[item.row] = item
            self.decryption_keys.add(item.row)

    def unselect_decryptkey(self, *args) :
        """ Unselect all decryption key."""
        selected_row = self.encryption_keys.get_selected_row()
        self.decryption_keys.unselect_all()
        self.encryption_keys.select_row(selected_row)

    def unselect_encryptkey(self, *args) :
        """ Unselect all encryption key."""
        selected_row = self.decryption_keys.get_selected_row()
        self.encryption_keys.unselect_all()
        self.decryption_keys.select_row(selected_row)

    def get_selected_key(self, get_type=False) :
        """ Return the selected GtkKey object."""
        selected_row = self.encryption_keys.get_selected_row()
        is_encrypt_key = True
        #if is not encrypt key, test is decrypt key
        if selected_row is None :
            selected_row = self.decryption_keys.get_selected_row()
            is_encrypt_key = False
        #if there is a key, return it, or return it and if is encrypt key
        if selected_row is not None :
            if not get_type :
                return self.keys[selected_row.get_child()]
            else :
                return (self.keys[selected_row.get_child()], is_encrypt_key)
        #else return none
        elif get_type :
            return None, None
        return None

    def manage_key(self, *args):
        key = self.get_selected_key()
        if key is not None :
            dialog = GtkKeySettings(key, (self.DEFAULT_ENCRYPT_KEYS, self.DEFAULT_DECRYPT_KEYS))
            dialog.run()
            self.set_listbox_keys()
        else :
            self.label.set_text(self.LABEL_MESSAGE['nokeyselected'])

    def add_new_key(self, *args) :
        dialog = GtkAddKey((self.DEFAULT_ENCRYPT_KEYS, self.DEFAULT_DECRYPT_KEYS))
        dialog.run()
        self.set_listbox_keys()

    def _get_file_input_output(self) :
        """ Return input and output path in tuple."""
        input_file = self.input.get_filename()
        if input_file is None : return (None, None)
        output_file = self.output.get_filename()
        if output_file is None and input_file.endswith(".vrnm") :
            output_file = input_file[:-5]
        elif output_file is None :
            output_file = "{}.vrnm".format(input_file)
        return (input_file, output_file)

    def update_openfile_infos(self, *args) :
        #active the output file selection
        self.outputbutton.set_sensitive(1)
        inpath, outpath = self._get_file_input_output()
        #set the label of input file
        file = File(inpath)
        self.inputlabel.set_text(
            self.LABEL_BASE['openfilesize'].replace("$size", file.get_size())
            )
        self.outputlabel.set_text(
            self.LABEL_BASE['resultfiledefault'].replace("$path", "{}".format(outpath))
            )

    def compute_manual_data(self, data, key, keyiscrypt) :
        """ Encrypt a manual text entry."""
        cypher_text = xor_text(data, key)
        if not keyiscrypt :
            try :
                cypher_text = cypher_text.decode('utf-8')
            except :
                pass
        out_dialogbox = GtkOutputDialog(cypher_text)
        out_dialogbox.run()

    def compute_file_data(self, input_path, output_path, key, keyiscrypt) :
        """ Encrypt a file entry with progress bar in Thread."""
        #init the progress bar
        final_size = os.path.getsize(input_path)
        if keyiscrypt :
            progress = GtkProgressDialog(output_path, final_size,
                title=self.LABEL_MESSAGE['progress_bar_encrypt'])
        else :
            progress = GtkProgressDialog(output_path, final_size,
                title=self.LABEL_MESSAGE['progress_bar_decrypt'])
        progress.start()
        #start encryptement
        process = Thread(target=xor_file, args=(input_path, key, output_path))
        process.start()
        #wait the end of cryptment
        tmp_key = "{}/.cuttingkey.tmp".format(os.path.dirname(key.path))
        while not os.path.exists(tmp_key) : sleep(0.1)
        progress.join()
        #show progress bar when rewriting key if len key > 10Mo
        final_size = len(key) - final_size #longueur_clé - longueur_fichier_de_sortie
        if final_size >= 10000000 :
            progress = GtkProgressDialog(tmp_key, final_size,
                title=self.LABEL_MESSAGE['progress_bar_keyrewriting'])
            progress.start()
            progress.join()
        process.join()

    def execute(self, *args) :
        """ This the method that run the vernam cypher."""
        #Get the selected key, else stop
        key, do_encrypt = self.get_selected_key(get_type=True)
        if key is None :
            self.label.set_text(self.LABEL_MESSAGE['nokeyselected'])
            return None

        #Get input data
        input_data, output_data = self._get_file_input_output()
        if input_data is None :
            input_data = self.entry.get_text()

            #if no input data : stop
            if input_data == "" :
                self.label.set_text(self.LABEL_MESSAGE['nodatainput'])
                return None

            #if there is more input data than length of key : stop
            if len(key) < len(input_data) :
                self.label.set_text(self.LABEL_MESSAGE['length_error'])
                return None

            #xor manual data
            self.compute_manual_data(input_data, key, do_encrypt)

        #or try xor file data
        else :
            #if key is less than file : stop
            if len(key) < os.path.getsize(input_data) :
                self.label.set_text(self.LABEL_MESSAGE['length_error'])
                return None

            #xor the file
            self.compute_file_data(input_data, output_data, key, do_encrypt)

        #reset key list to update size informations
        self.set_listbox_keys()
        self.label.set_text(self.LABEL_MESSAGE['computing_is_end'])

if __name__ == "__main__" :
    VernamGui = VernamGui()
    Gtk.main()
